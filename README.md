## genderize

Simple script used to call the [genderize API](https://genderize.io/), in order to get the gender associated with a first name. 

The input and output files are part of a test I did with greek names.

**Disclaimer**: by creating this script and using this service, I do not imply that the perceived and experienced gender of a person is determined by their first name. However, there are cases (e.g. the one I faced when I searched for such a service), when one needs to automatically fill in a binary gender field in a database.
