#/bin/bash/

#genderize

while read line
do
    echo "Looking for "$line"..."
    result=`curl -s https://api.genderize.io/?name=$line | grep gender\":\".*probabi -o | sed s/gender\"://g | sed s/,\"probabi//g | sed s/\"//g`
    if [ -z "$result" ]; then
        nameOne=`echo $line | cut -f 1 -d -` 
        echo "Didn't find "$line", searching for "$nameOne  
        result=`curl -s https://api.genderize.io/?name=$nameOne | grep gender\":\".*probabi -o | sed s/gender\"://g | sed s/,\"probabi//g | sed s/\"//g`
    fi
    if [ -z "$result" ]; then
        nameTwo=`echo $line | cut -f 2 -d -` 
        echo "Didn't find "$nameOne", searching for "$nameTwo
        result=`curl -s https://api.genderize.io/?name=$nameTwo | grep gender\":\".*probabi -o | sed s/gender\"://g | sed s/,\"probabi//g | sed s/\"//g`
    fi
    echo $line";"$result >> resultFile
done < $1